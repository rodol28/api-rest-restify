const restify = require('restify');
const server = restify.createServer();


// settings
server.use(restify.plugins.acceptParser(server.acceptable)); //para que pueda aceptar ciertos tipos de cabeceras
server.use(restify.plugins.queryParser()); //para que puerda convertir las querys
server.use(restify.plugins.bodyParser()); //para que pueda entender los datos que vienen en formato JSON


const users = {
    1: {
        "name": "John",
        "lastname": "Doe"
    },
    2: {
        "name": "Jane",
        "lastname": "Smith"
    }
};

let usersCount = 2;

// routes
server.get('/users', (req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    res.writeHead(200);
    res.end(JSON.stringify(users));
});

server.get('/users/:id', (req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    res.writeHead(200);
    res.end(JSON.stringify(users[parseInt(req.params.id)]));
});

server.post('/users', (req, res, next) => {
    const user = req.body;
    usersCount++;
    user.id = usersCount;
    users[user.id] = user;
    res.setHeader('Content-Type', 'application/json');
    res.writeHead(200);
    res.end(JSON.stringify(user));
});

server.put("/users/:id", (req, res, next) => {
    const user = users[parseInt(req.params.id)];
    const update = req.body;

    for (let field in update) {
        user[field] = update[field];
    }

    res.setHeader('Content-Type', 'application/json');
    res.writeHead(200);
    res.end(JSON.stringify(user));
});

server.del('/users/:id', (req, res, next) => {
    delete users[parseInt(req.params.id)];
    res.setHeader('Content-Type', 'application/json');
    res.writeHead(200);
    res.end(JSON.stringify(users));
});


// start server
server.listen(3000, () => {
    console.log('Server listening on port 3000');
});